
function setup() {
  createCanvas(800,600);
  background(220);
}

function draw(){

  // 2DPrimitives
  // rect(mouseX,mouseY,80,80);
  // console.log("FrameCount"+frameCount);

  // Curve 
  // fill(255,0,0);
  // bezier(100,100,150,200,350,200,400,100);

  // translate(100,100);
  // curve(150,0,100,100,400,100,350,0);

  // translate(100,100);
  // curve(5, 26, 5, 26, 73, 24, 73, 61);

  
  // Vertex(頂點)
  // 正方形
  // beginShape();
  // vertex(20,20);
  // vertex(60,20);
  // vertex(60,60);
  // vertex(40,60);
  // vertex(40,40);
  // vertex(20,40);
  // endShape(CLOSE);

  // 菱形
  // beginShape();
  // translate(100,100);
  // for(var a = 0 ; a < TWO_PI + 1; a += TWO_PI/4){
  //   x = cos(a) * 50;
  //   y = sin(a) * 50;
  //   vertex(x,y);
  // }
  // endShape();
  // scale(2);

  // 八邊形
  beginShape();
  translate(100,100);
  for(var a = 0 ; a < TWO_PI + 1; a += TWO_PI/8){
    x = cos(a) * 50;
    y = sin(a) * 50;
    vertex(x,y);
  }
  endShape();
  scale(2);






}
