var x=0,y=0;


function setup() {
  createCanvas(1000,800);
}

function draw(){

  background(220);

  push();
    rect(175,200,50,200);

    ellipseMode(CENTER);
    fill(50,125,255);
    ellipse(200,200,100,100);

    translate(200,200);

    fill(10,125,255);
    ellipse(0,0,50,50);

    rotate(frameCount*0.01);
    rectMode(CENTER);
    rect(0,0,200,40);
    rect(0,0,40,200);
  pop();


  translate(250,0)

  // Second
  push();

    rect(175,200,50,200);

    ellipseMode(CENTER);
    fill(50,125,255);
    ellipse(200,200,100,100);

    translate(200,200);

    fill(10,125,255);
    ellipse(0,0,50,50);

    rotate(frameCount*0.1);
    rectMode(CENTER);
    rect(0,0,200,40);
    rect(0,0,40,200);
  pop();
}
