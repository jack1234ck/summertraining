var x=0,y=0;


function setup() {
  createCanvas(1200,800);
}

function draw(){

  background(220);

  push(); 
    ellipseMode(CENTER);
    translate(300,300);

    fill(255,125,255);
    ellipse(0,0,100,100);

    rotate(frameCount*0.02);
    ellipse(100,100,30,30);

    translate(100,100);

    ellipseMode(CENTER);
    rotate(frameCount*0.2);
    ellipse(40,40,30,30);

  pop();
}
