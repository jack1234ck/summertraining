
var inp,colorP,wordsize,div;

function setup() {

  noCanvas();

  div = createDiv();
  div.id('div');
  div.style("background-color","rgba(34,34,50,0.1)");
  div.style("display","flex");
  div.style("flex-direction","row");
  background(200);

  inp = createInput('');
  inp.style('margin-left','100px');
  inp.style('width','150px');
  inp.attribute('align','left');
  inp.id(inp);
  inp.input(myInputEvent);

  wordsize = createP("請輸入文字大小！");
  wordsize.id('wordone');
  wordsize.child(inp);
  wordsize.attribute('align','left');


  colorP = createColorPicker('#ff0000');
  colorP.child();
  colorP.input(setShade);
  colorP.id('colorp');

  button = createButton('Change Size');
  button.mousePressed(change);
  button.id('button');

  div.child("inp");
  div.child("wordone");
  div.child("colorp");
  div.child("button");

}

function myInputEvent(){
  console.log('You are typing :',this.value());
}

function setShade() {
  wordsize.style('color',colorP.color());
  console.log('You are choosing color to be : ', this.value());
}

function change() {

  size = inp.value();
  wordsize.style("font-size",size + "px");
  console.log('You are choosing size to be : ', this.value());
}


