var x=0,y=0;


function setup() {
  createCanvas(600,600,WEBGL);
}

function draw(){

  background(220);

  rotateX(50);
  rotateY(frameCount*0.01);
  rotateZ(500);
  fill(255,0,0)
  cone(100,100,100);
  translate(0,-100);
  box(60,100,60);

  camera(0,-50,mouseX,0,0,0,0,1,0);
  
}
