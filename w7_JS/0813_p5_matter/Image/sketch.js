var img;

function preload(){
  img = loadImage('LINE.png');
}

function setup() {
  createCanvas(600,600);
}

function draw(){
  background(220);
  image(img,100,100,img.width/20,img.height/20);
  scale(2);
  image(img,100,100,img.width/20,img.height/20);
}
