var x=0,y=0;
var dirX = 0;
var dirY = 0;

function setup() {
  createCanvas(800,600);
}

function draw(){

  background(220);
  var colorR = frameCount/4%255;
  var colorG = frameCount/3%255;
  var colorB = frameCount/2%255;

  if(x>760|| mouseX == x ){
    dirX = -5;
    // fill(13,90,8);
    fill(colorR,colorG,colorB);
  }else if(x<=0){
    dirX = 5;
    // fill(3,230,48);
    fill(colorR,colorG,colorB);

  }

  if(y>560 || mouseY == y ){
    dirY = -5;
    // fill(180,12,233);
    fill(colorR,colorG,colorB);

  }else if(y<=0){
    dirY = 5;  
    // fill(33,60,218);
    fill(colorR,colorG,colorB);
  }

  // rect(x+=dirX, y+=dirY, 40, 40);
  ellipse(x+=dirX, y+=dirY, 60, 60);

  ellipse(mouseX, mouseY, 30, 30);
  
}
