package com.example.androidbasicelement;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    ListView listview1;
    Spinner spinner1;
    TextView datatextView;

    String[] datas = {"一", "二", "三", "四"};
    String[] datas4Spinner = {"資料庫", "統計", "管理學", "計概"};
    ArrayAdapter arrayAdapter1;
    ArrayAdapter arrayAdapter4Spinner;

    public void setview(){
        listview1 = (ListView) findViewById(R.id.listview);
        spinner1 = (Spinner) findViewById(R.id.spinner);
        datatextView = (TextView) findViewById(R.id.data);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setview();

        //AlertDialog
        final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(MainActivity.this);
        alertBuilder.setTitle("提示");
        alertBuilder.setMessage("這是一個提示對話框！");
        alertBuilder.setPositiveButton("確定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                alertBuilder.setMessage("Hello");
            }
        });

        alertBuilder.setNegativeButton("結束", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                alertBuilder.setMessage("Bye");
            }
        });

        AlertDialog ad = alertBuilder.create();
        ad.show();


        //ListView
        arrayAdapter1 = new ArrayAdapter(this, android.R.layout.simple_list_item_1, datas);
        listview1.setAdapter(arrayAdapter1);
        listview1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(MainActivity.this, "你在 'ListView' 裡選擇了" + datas[i] + "，位於第" + (i + 1) + "列。", Toast.LENGTH_SHORT).show();

                alertBuilder.setTitle("ListView");
                alertBuilder.setMessage("你選擇了 '" + datas[i] + "'");
                AlertDialog ad = alertBuilder.create();
                ad.show();

            }
        });

        //Spinner
        arrayAdapter4Spinner = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, datas4Spinner);
        spinner1.setAdapter(arrayAdapter4Spinner);
        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(MainActivity.this, "你在 'Spinner' 裡選擇了" + datas4Spinner[i] + "，位於第" + (i + 1) + "列。", Toast.LENGTH_SHORT).show();
                datatextView.setText(datas4Spinner[i]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
}
