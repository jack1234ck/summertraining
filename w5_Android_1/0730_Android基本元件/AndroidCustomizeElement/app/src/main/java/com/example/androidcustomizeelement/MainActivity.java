package com.example.androidcustomizeelement;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    String[] NameList = {"陳振東", "馬麗菁", "張朝旭"};
    String[] PhoneList = {"037-381515", "037-381510", "037-381520"};
    String[] JobList = {"教授", "教授 兼 管理學院院長", "副教授 兼 圖書館系統管理組長"};

    int[] pic = {R.drawable.device, R.drawable.woman, R.drawable.man};
    int[] TimeList = {0, 0, 0};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Activity2");


        ListView lv = (ListView) findViewById(R.id.listvieww);

        BaseAdapter adpter = new BaseAdapter() {
            @Override
            public int getCount() {
                return NameList.length;
            }

            @Override
            public Object getItem(int i) {
                return i;
            }

            @Override
            public long getItemId(int i) {
                return i;
            }

            @Override
            public View getView(int i, View view, ViewGroup viewGroup) {
                View LL = View.inflate(MainActivity.this, R.layout.layout1, null);

                int times;

                ImageView img = LL.findViewById(R.id.layoutimg);
                TextView name = LL.findViewById(R.id.nametext);
                TextView phone = LL.findViewById(R.id.phonetext);
                TextView job = LL.findViewById(R.id.jobtext);

                img.setImageResource(pic[i]);
                name.setText(NameList[i]);
                phone.setText(PhoneList[i]);
                job.setText(JobList[i]);

                Log.e("Name", NameList[i]);

                return LL;
            }
        };

        lv.setAdapter(adpter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TimeList[i]++;
                Toast.makeText(MainActivity.this, "點擊" + NameList[i] + "，次數：" + TimeList[i] + "次", Toast.LENGTH_SHORT).show();
            }
        });


    }


    // 產生menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);     //產生選單並呼叫你的menu 作為選單樣式 
        return true;
    }

    // 按下選項後要做的事
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        String n = item.toString();
        Toast.makeText(MainActivity.this, "你選擇了 'Menu' 裡的" + n, Toast.LENGTH_SHORT).show();
        return true;


//        int id = item.getItemId();   //取得menu裡的item 的id
//        // 根據取得的id判斷要做甚麼事
//        if (id == R.id.id1) {
//            Toast.makeText(MainActivity.this, "你選擇了 'Menu' 裡的" , Toast.LENGTH_SHORT);
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
    }


}


