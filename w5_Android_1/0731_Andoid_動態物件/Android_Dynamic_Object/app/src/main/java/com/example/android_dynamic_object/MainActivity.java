package com.example.android_dynamic_object;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity {

    int size = 4;
    Button ArrayButton[][];

    String one, two;
    int[] All = new int[16];

    Button BtOpen = null;

    LinearLayout LayoutOne, LayoutTwo, LayoutThree, LayoutFour;

    int colorArray[] = {Color.parseColor("#e6ffe6"), Color.parseColor("#ffffcc")};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        LayoutOne = findViewById(R.id.row1);
        LayoutTwo = findViewById(R.id.row2);
        LayoutThree = findViewById(R.id.row3);
        LayoutFour = findViewById(R.id.row4);

        CreateButton();
    }

    void CreateButton() {

        ArrayButton = new Button[size][size];

        for (int allnum = 0; allnum < 16; allnum++) {
            All[allnum] = ((int) (Math.random() * 16) + 1);
            for (int check = 0; check < allnum; ) {
                if (All[check] == All[allnum]) {
                    All[allnum] = ((int) (Math.random() * 16) + 1);
                    check = 0;
                } else check++;
            }
        }

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                ArrayButton[i][j] = new Button(this);
                ArrayButton[i][j].setText("*");
                if (i == 0) {
                    LayoutOne.addView(ArrayButton[i][j]);
                } else if (i == 1) {
                    LayoutTwo.addView(ArrayButton[i][j]);
                } else if (i == 2) {
                    LayoutThree.addView(ArrayButton[i][j]);
                } else if (i == 3) {
                    LayoutFour.addView(ArrayButton[i][j]);
                }

                int num = 0;
                if (All[i * 4 + j] > 8) {
                    num = All[i * 4 + j] % 8 + 1;
                } else {
                    num = All[i * 4 + j];
                }

                final int finalI = i;
                final int finalJ = j;
                final int finalNum = num;
                ArrayButton[i][j].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ArrayButton[finalI][finalJ].setText(String.valueOf(finalNum));
                        if (BtOpen == null) {//翻第一張
                            BtOpen = ArrayButton[finalI][finalJ];
                            one = String.valueOf(ArrayButton[finalI][finalJ].getText());

                        } else {//翻第二張
                            two = String.valueOf(ArrayButton[finalI][finalJ].getText());
                            if (BtOpen == ArrayButton[finalI][finalJ]) { //重複點取同個按鍵
                                Log.e("配對", "錯誤（重複點取同個按鍵）(" + BtOpen.getText() + "," + ArrayButton[finalI][finalJ].getText() + ")");
                                final Handler cardHandler = new Handler();
                                cardHandler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        BtOpen.setText("*");
                                        ArrayButton[finalI][finalJ].setText("*");
                                        BtOpen = null;
                                    }
                                }, 500);

                            } else if (one.equals(two)) { //配對成功
                                Log.e("配對", "成功 (" + BtOpen.getText() + "," + ArrayButton[finalI][finalJ].getText() + ")");
                                BtOpen.setEnabled(false);
                                ArrayButton[finalI][finalJ].setEnabled(false);
                                BtOpen.setBackgroundColor(Color.parseColor("#e6ffe6"));
                                ArrayButton[finalI][finalJ].setBackgroundColor(Color.parseColor("#e6ffe6"));
                                BtOpen = null;
                            } else { //配對失敗
                                Log.e("配對", "失敗 (" + BtOpen.getText() + "," + ArrayButton[finalI][finalJ].getText() + ")");
                                final Handler cardHandler = new Handler();
                                cardHandler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        BtOpen.setText("*");
                                        ArrayButton[finalI][finalJ].setText("*");
                                        BtOpen = null;
                                    }
                                }, 500);
                            }
                        }
                    }
                });
            }
        }


    }


}
