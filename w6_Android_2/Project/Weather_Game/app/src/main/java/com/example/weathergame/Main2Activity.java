package com.example.weathergame;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Space;
import android.widget.Spinner;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {

    TextView[] tv_AArray = new TextView[8];
    TextView[] tv_BArray = new TextView[8];
    TextView[] tv_CArray = new TextView[8];
    TextView[] tv_DArray = new TextView[8];
    TextView tv_AAns, tv_BAns, tv_CAns, tv_DAns;
    TextView tv_Result;

    Spinner[] SpArray = new Spinner[8];
    final String[] Weather = {"🌞", "☔️"};
    Button bt_Check, bt_Exit;


    int[] A = new int[7];
    int[] B = new int[7];
    int[] C = new int[7];
    int[] D = new int[7];
    int[] Ans = new int[7];

    int AToAns, BToAns, CToAns, DToAns;

    public void view() {

        tv_AArray[1] = (TextView) findViewById(R.id.textView_A1);
        tv_AArray[2] = (TextView) findViewById(R.id.textView_A2);
        tv_AArray[3] = (TextView) findViewById(R.id.textView_A3);
        tv_AArray[4] = (TextView) findViewById(R.id.textView_A4);
        tv_AArray[5] = (TextView) findViewById(R.id.textView_A5);
        tv_AArray[6] = (TextView) findViewById(R.id.textView_A6);
        tv_AArray[7] = (TextView) findViewById(R.id.textView_A7);

        tv_BArray[1] = (TextView) findViewById(R.id.textView_B1);
        tv_BArray[2] = (TextView) findViewById(R.id.textView_B2);
        tv_BArray[3] = (TextView) findViewById(R.id.textView_B3);
        tv_BArray[4] = (TextView) findViewById(R.id.textView_B4);
        tv_BArray[5] = (TextView) findViewById(R.id.textView_B5);
        tv_BArray[6] = (TextView) findViewById(R.id.textView_B6);
        tv_BArray[7] = (TextView) findViewById(R.id.textView_B7);

        tv_CArray[1] = (TextView) findViewById(R.id.textView_C1);
        tv_CArray[2] = (TextView) findViewById(R.id.textView_C2);
        tv_CArray[3] = (TextView) findViewById(R.id.textView_C3);
        tv_CArray[4] = (TextView) findViewById(R.id.textView_C4);
        tv_CArray[5] = (TextView) findViewById(R.id.textView_C5);
        tv_CArray[6] = (TextView) findViewById(R.id.textView_C6);
        tv_CArray[7] = (TextView) findViewById(R.id.textView_C7);

        tv_DArray[1] = (TextView) findViewById(R.id.textView_D1);
        tv_DArray[2] = (TextView) findViewById(R.id.textView_D2);
        tv_DArray[3] = (TextView) findViewById(R.id.textView_D3);
        tv_DArray[4] = (TextView) findViewById(R.id.textView_D4);
        tv_DArray[5] = (TextView) findViewById(R.id.textView_D5);
        tv_DArray[6] = (TextView) findViewById(R.id.textView_D6);
        tv_DArray[7] = (TextView) findViewById(R.id.textView_D7);

        SpArray[1] = (Spinner) findViewById(R.id.spinner1);
        SpArray[2] = (Spinner) findViewById(R.id.spinner2);
        SpArray[3] = (Spinner) findViewById(R.id.spinner3);
        SpArray[4] = (Spinner) findViewById(R.id.spinner4);
        SpArray[5] = (Spinner) findViewById(R.id.spinner5);
        SpArray[6] = (Spinner) findViewById(R.id.spinner6);
        SpArray[7] = (Spinner) findViewById(R.id.spinner7);

        ArrayAdapter<String> WeatherList = new ArrayAdapter<>(Main2Activity.this,
                android.R.layout.simple_spinner_dropdown_item,
                Weather);

        for (int i = 1; i < 8; i++) {
            SpArray[i].setAdapter(WeatherList);
        }


        tv_AAns = (TextView) findViewById(R.id.textView_AToAns);
        tv_BAns = (TextView) findViewById(R.id.textView_BToAns);
        tv_CAns = (TextView) findViewById(R.id.textView_CToAns);
        tv_DAns = (TextView) findViewById(R.id.textView_DToAns);

        bt_Exit = (Button) findViewById(R.id.button_exit);
        tv_Result = (TextView) findViewById(R.id.textView_Result);
        bt_Check = (Button) findViewById(R.id.button_check);


    }

    public void AnsRestart() {
        for (int i = 0; i < 7; i++) {
            Ans[i] = (int) (Math.random() * 2);
            Log.e("Ans[" + i + "]", String.valueOf(Ans[i]));

        }

    }

    public void ABCDRestart() {

        AToAns = 0;
        BToAns = 0;
        CToAns = 0;
        DToAns = 0;

        for (int i = 0; i < 7; i++) {
            A[i] = (int) (Math.random() * 2);
            B[i] = (int) (Math.random() * 2);
            C[i] = (int) (Math.random() * 2);
            D[i] = (int) (Math.random() * 2);

            if (A[i] == Ans[i]) {
                AToAns += 1;
            }
            if (B[i] == Ans[i]) {
                BToAns += 1;
            }
            if (C[i] == Ans[i]) {
                CToAns += 1;
            }
            if (D[i] == Ans[i]) {
                DToAns += 1;
            }
        }
    }

    public void SetLayout() {
        for (int i = 0; i < 7; i++) {

            if (A[i] == 0) {
                tv_AArray[i + 1].setText("🌞️");
            } else if (A[i] == 1) {
                tv_AArray[i + 1].setText("☔️");
            }

            if (B[i] == 0) {
                tv_BArray[i + 1].setText("🌞️");
            } else if (B[i] == 1) {
                tv_BArray[i + 1].setText("☔️");
            }

            if (C[i] == 0) {
                tv_CArray[i + 1].setText("🌞️");
            } else if (C[i] == 1) {
                tv_CArray[i + 1].setText("☔️️");
            }

            if (D[i] == 0) {
                tv_DArray[i + 1].setText("🌞️");
            } else if (D[i] == 1) {
                tv_DArray[i + 1].setText("☔️");
            }

        }

        tv_AAns.setText(String.valueOf(AToAns));
        tv_BAns.setText(String.valueOf(BToAns));
        tv_CAns.setText(String.valueOf(CToAns));
        tv_DAns.setText(String.valueOf(DToAns));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        view();
        AnsRestart();
        ABCDRestart();
        SetLayout();

        bt_Check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int player = 0;

                for (int i = 1; i < 8; i++) {
                    int w = 0;
                    if (SpArray[i].getSelectedItem().toString().equals("🌞️")) {
                        w = 0;
                    } else if (SpArray[i].getSelectedItem().toString().equals("☔️")) {
                        w = 1;
                    }

                    if (Ans[i - 1] == w) {
                        player += 1;
                    }
                }
                tv_Result.setText(String.valueOf(player));

                if (player == 7) {
                    tv_Result.setText("遊戲結束" + String.valueOf(player));
                    bt_Check.setEnabled(false);
                }
            }
        });

        bt_Exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(Main2Activity.this, MainActivity.class);
                startActivity(intent);
            }
        });


    }
}
