package com.example.whac_a_mole;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    int size = 3;

    ImageButton ArrayButton[][];
    LinearLayout LayoutOne, LayoutTwo, LayoutThree;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        LayoutOne = findViewById(R.id.Column1);
        LayoutTwo = findViewById(R.id.Column2);
        LayoutThree = findViewById(R.id.Column3);

        CreateButton();

    }

    void CreateButton() {
        ArrayButton = new ImageButton[size][size];

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                ArrayButton[i][j] = new ImageButton(this);

//                ArrayButton[i][j].setText("*");
//                ArrayButton[i][j].setHeight(200);
//                ArrayButton[i][j].setWidth(200);

                if (i == 0) {
//                    ArrayButton[i][j].setBackgroundResource(R.drawable.apple);
                    LayoutOne.addView(ArrayButton[i][j]);
                } else if (i == 1) {
                    ArrayButton[i][j].setBackgroundResource(R.drawable.bomb);
//                    ArrayButton[i][j].size
//                    ArrayButton[i][j].setHeight(200);
//                    ArrayButton[i][j].setWidth(200);



//                    ArrayButton[i][j].getLayoutParams().width = 150;
//                    ArrayButton[i][j].getLayoutParams().height = 150;


                    LayoutTwo.addView(ArrayButton[i][j]);
                } else if (i == 2) {
                    LayoutThree.addView(ArrayButton[i][j]);
                }
                final int finalI = i;
                final int finalJ = j;
                ArrayButton[i][j].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(view.getContext(), "hello:" + finalI + ":" + finalJ, Toast.LENGTH_SHORT).show();
                    }
                });

            }
        }


    }
}
