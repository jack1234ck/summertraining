package com.example.socket;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

public class Main2Activity extends AppCompatActivity {

    TextView Name, History;
    Button bt_exit, bt_send;
    EditText et_Msg;
    Spinner sp_status, sp_people;

    String[] status = {"所有人", "私訊"};
    String[] people = {"One", "Two", "Three"};

    static Socket Client;
    static String Record, NameString;
    static String CurrentStatus;
    static String CurrentPeople;


    public void view() {

        Name = (TextView) findViewById(R.id.textView_Name);
        History = (TextView) findViewById(R.id.textView_History);
        bt_exit = (Button) findViewById(R.id.button_exit);
        bt_send = (Button) findViewById(R.id.button_send);
        et_Msg = (EditText) findViewById(R.id.editText_Msg);
        sp_status = (Spinner) findViewById(R.id.spinner_status);
        sp_people = (Spinner) findViewById(R.id.spinner_people);

        ArrayAdapter<String> statusList = new ArrayAdapter<>(Main2Activity.this,
                android.R.layout.simple_spinner_dropdown_item,
                status);
        sp_status.setAdapter(statusList);

        ArrayAdapter<String> peopleList = new ArrayAdapter<>(Main2Activity.this, android.R.layout.simple_spinner_dropdown_item,
                people);
        sp_people.setAdapter(peopleList);

        Intent intent2 = getIntent();
        Bundle bundle = intent2.getExtras();
        NameString = bundle.getString("Name");
        Log.e("Name", NameString);
        Name.setText(NameString);

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        view();


        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Client = new Socket("172.21.14.94", 500);
                    DataInputStream dataInputS = new DataInputStream(Client.getInputStream());
                    DataOutputStream dataOutputS = new DataOutputStream(Client.getOutputStream());
                    if (Client.isConnected()) {
                        dataOutputS = new DataOutputStream(Client.getOutputStream());
                        dataOutputS.writeUTF(NameString);
                        Log.e("Client", "Connected.");

                    } else {
                        Log.e("Client", "Connect Failed.");
                    }
                    while (Client.isConnected()) {
                        dataInputS = new DataInputStream(Client.getInputStream());
                        Record = dataInputS.readUTF();
                        Log.e("History", Record);

                        Message showmsg = new Message();
                        show.sendMessage(showmsg);
                    }

                } catch (UnknownHostException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        sp_status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                CurrentStatus = status[i];
                if (i == 0) {
                    sp_people.setEnabled(false);
                } else {
                    sp_people.setEnabled(true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        sp_people.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                CurrentPeople = people[i];
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        bt_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String Message;
                Message = et_Msg.getText().toString();

                try {
                    if (CurrentStatus.equals("所有人")) {
                        DataOutputStream dataOutputS = new DataOutputStream(Client.getOutputStream());
                        dataOutputS = new DataOutputStream(Client.getOutputStream());
                        dataOutputS.writeUTF("ALL:" + Message);
                    } else if (CurrentStatus.equals("私訊")) {
                        DataOutputStream dataOutputS = new DataOutputStream(Client.getOutputStream());
                        dataOutputS = new DataOutputStream(Client.getOutputStream());

                        dataOutputS.writeUTF(CurrentPeople + ":" + Message);
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
        });

        bt_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                System.out.println("退出聊天室");
                try {
                    DataOutputStream dataOutputS = new DataOutputStream(Client.getOutputStream());
                    dataOutputS.writeUTF("BYE:BYE");
                    Client.shutdownInput();
                    Client.shutdownOutput();
                    Client.close();
                    Intent IntentObj = new Intent();
                    IntentObj.setClass(Main2Activity.this, MainActivity.class);
                    Main2Activity.this.startActivity(IntentObj);
                    Main2Activity.this.finish();
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
        });

    }

    @SuppressLint("HandlerLeak")
    Handler show = new Handler() {
        public void handleMessage(Message msg) {
            History.append(Record + "\n");
        }
    };
}
