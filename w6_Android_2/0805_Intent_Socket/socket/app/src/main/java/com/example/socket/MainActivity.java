package com.example.socket;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText et_Name;
    Button bt_In;
    String name;

    public void view() {

        et_Name = (EditText) findViewById(R.id.editText_Name);
        bt_In = (Button) findViewById(R.id.button_InChat);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        view();

        bt_In.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                name = et_Name.getText().toString();

                if (name.matches("")) {
                    Toast.makeText(MainActivity.this, "暱稱不可為空值，請重新輸入。", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    Intent intent = new Intent();
                    Bundle bundle = new Bundle();
                    intent.setClass(MainActivity.this, Main2Activity.class);
                    bundle.putString("Name", name);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }

            }
        });


    }
}
