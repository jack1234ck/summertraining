package com.example.intent;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    EditText et_Name, et_Age;
    TextView tv_Address;
    Button bt_topage2;
    String Name, Age;

    public void view() {
        et_Name = (EditText) findViewById(R.id.editText_Name);
        et_Age = (EditText) findViewById(R.id.editText_Age);
        tv_Address = (TextView) findViewById(R.id.editText_Address);

        bt_topage2 = (Button) findViewById(R.id.button_ToPage2);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        view();

        bt_topage2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Name = et_Name.getText().toString();
                Age = et_Age.getText().toString();

                Intent intent = new Intent();
                Bundle bundle = new Bundle();
                intent.setClass(MainActivity.this, Main2Activity.class);

                bundle.putString("Name", Name);
                bundle.putString("Age", Age);
                intent.putExtras(bundle);
                startActivity(intent);

            }
        });

        Intent reternIntent = getIntent();
        Bundle bundle2 = reternIntent.getExtras();
        if (bundle2 != null && bundle2.containsKey("Address")) {
            String address = bundle2.getString("Address");
            tv_Address.setText(address);
        }

    }


}
