package com.example.intent;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {

    TextView tv_Name, tv_Age;
    String SelectedCity;

    ListView lv_city;
    ListAdapter la_city;
    String city[] = new String[]{"新北市", "台北市", "台中市", "台南市", "高雄市"};

    Button bt_ToOne;


    public void view() {

        tv_Name = (TextView) findViewById(R.id.Name);
        tv_Age = (TextView) findViewById(R.id.Age);

        lv_city = (ListView) findViewById(R.id.listview_city);
        la_city = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, city);
        lv_city.setAdapter(la_city);

        bt_ToOne = (Button) findViewById(R.id.buttonToOne);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        view();

        Intent intentp2 = getIntent();
        Bundle bundle = intentp2.getExtras();

        String getName = bundle.getString("Name");
        Log.e("GetName", getName);
        String getAge = bundle.getString("Age");
        Log.e("GetAge", getAge);


        tv_Name.setText(getName);
        tv_Age.setText(getAge);

        lv_city.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                ListView listView = (ListView) adapterView;
                SelectedCity = listView.getItemAtPosition(i).toString();

            }
        });

        bt_ToOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                Bundle bundle1 = new Bundle();

                intent.setClass(Main2Activity.this, MainActivity.class);
                Log.e("SelectedCity",SelectedCity);
                bundle1.putString("Address", SelectedCity);
                intent.putExtras(bundle1);
                startActivity(intent);
            }
        });

    }
}
