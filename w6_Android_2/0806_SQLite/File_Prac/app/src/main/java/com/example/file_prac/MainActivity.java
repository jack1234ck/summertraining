package com.example.file_prac;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class MainActivity extends AppCompatActivity {


    Button bt_Read, bt_Write, bt_Delete;
    EditText et_Content;
    TextView tv_RContent;


    public void view() {

        bt_Read = (Button) findViewById(R.id.button_Read);
        bt_Delete = (Button) findViewById(R.id.button_Delete);
        bt_Write = (Button) findViewById(R.id.button_Write);

        et_Content = (EditText) findViewById(R.id.editText_Content);

        tv_RContent = (TextView) findViewById(R.id.textView_ReadContent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        view();

        bt_Write.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FileOutputStream fos = null;
                try {
                    fos = openFileOutput("Test.txt", Context.MODE_PRIVATE);
                    fos.write(et_Content.getText().toString().getBytes());
                    fos.close();
                    File file = new File(getFilesDir() + "/" + fos);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        bt_Read.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FileInputStream fis = null;
                BufferedInputStream buffered = null;
                try {
                    fis = openFileInput("Test.txt");
                    buffered = new BufferedInputStream(fis);
                    byte[] BufferedByte = new byte[200];
                    tv_RContent.setText("");
                    while (true) {
                        int flag = buffered.read(BufferedByte);
                        if (flag == -1) break;
                        else {
                            tv_RContent.append(new String(BufferedByte), 0, flag);
                        }
                    }
                    buffered.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        bt_Delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                File file = new File(getFilesDir() + "/" + "Test.txt");
                if (file.exists()){
                    file.delete();
                    Toast.makeText(getBaseContext(),"File Deleted.",Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(getBaseContext(),"File doesn't Exist.",Toast.LENGTH_SHORT).show();

                }
            }
        });


    }
}
