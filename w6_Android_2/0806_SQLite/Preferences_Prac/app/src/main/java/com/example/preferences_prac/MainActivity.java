package com.example.preferences_prac;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    SharedPreferences setting;

    Button bt_save, bt_clear, bt_load;
    EditText et_Name, et_Phone, et_Sex;
    TextView tv_Result;
    String Name, Phone, Sex;

    public void view() {

        et_Name = (EditText) findViewById(R.id.editText_Name);
        et_Phone = (EditText) findViewById(R.id.editText_Phone);
        et_Sex = (EditText) findViewById(R.id.editText_Sex);

        bt_save = (Button) findViewById(R.id.button_Save);
        bt_clear = (Button) findViewById(R.id.button_Clear);
        bt_load = (Button) findViewById(R.id.button_Load);

        tv_Result = (TextView) findViewById(R.id.textView_Result);


    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setting = getSharedPreferences("SharedName", MODE_PRIVATE);

        view();
        bt_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Name = et_Name.getText().toString();
                Phone = et_Phone.getText().toString();
                Sex = et_Sex.getText().toString();

                setting.edit().putString("Name", Name)
                        .putString("Phone", Phone)
                        .putString("Sex", Sex).apply();

            }
        });

        bt_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                setting.edit().putString("Name", null)
                        .putString("Phone", null)
                        .putString("Sex", null).apply();


            }
        });

        bt_load.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Name = setting.getString("Name", null);
                Phone = setting.getString("Phone", null);
                Sex = setting.getString("Sex", null);

                tv_Result.setText("Name:" + Name + "\nPhone:" + Phone + "\nSex:" + Sex);

            }
        });

    }
}
