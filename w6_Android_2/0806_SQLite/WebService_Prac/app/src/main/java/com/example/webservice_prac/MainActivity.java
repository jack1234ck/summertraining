package com.example.webservice_prac;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    URL url;
    HttpURLConnection conn;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    url = new URL("http://localhost:8080/phpmyadmin/sql.php");
                    conn = (HttpURLConnection) url.openConnection();
                    conn.connect();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }).start();

        try {
            InputStream is = conn.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuilder response = new StringBuilder();
            while ((line = br.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            br.close();
            Log.e("read",response.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }



    }
}
