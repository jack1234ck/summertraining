package com.example.hardware_prac;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    ListView lvs;
    SensorManager sm;
    List<Sensor> sensorList;

    //宣告一個sensor物件
    Sensor sr;
    TextView tv_A, tv_L;


    public void setSensorList() {
        lvs = (ListView) findViewById(R.id.ListView_Sensor);
        sm = (SensorManager) getSystemService(SENSOR_SERVICE);
        sensorList = sm.getSensorList(Sensor.TYPE_ALL);
        ArrayList<String> arrayList_Sensor = new ArrayList<>();
        for (int i = 0; i < sensorList.size(); i++) {
            arrayList_Sensor.add(sensorList.get(i).getName());
        }
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, arrayList_Sensor);
        lvs.setAdapter(arrayAdapter);
    }

    public void setAccelerometer() {
        tv_A = (TextView) findViewById(R.id.textView_XYZ);
        //取得sensor服務並使用
        sm = (SensorManager) getSystemService(SENSOR_SERVICE);
        //TYPE_ACCELEROMETER類型
        sr = sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

    }

    public void setLight() {
        tv_L = (TextView) findViewById(R.id.textView_Light);
        sr = sm.getDefaultSensor(Sensor.TYPE_LIGHT);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setSensorList();
        setAccelerometer();
//        setLight();

    }


    //當sensor數值改變時使用
    @SuppressLint("SetTextI18n")
    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {

        //XYZ 軸顯示
        tv_A.setText("X軸為：" + sensorEvent.values[0]+ "\nY軸為" + sensorEvent.values[1] + "\nZ軸為" + sensorEvent.values[2]);

//        //光照感測器 顯示
//        Sensor lights_sensor = sensorEvent.sensor;
//        String data = null;
//        data = "名字：" + lights_sensor.getName() + "\n";
//        data += "供應商：" + lights_sensor.getVendor() + "\n";
//        data += "感光最大範圍：" + lights_sensor.getMaximumRange() + "\n";
//        data += "感光值：" + sensorEvent.values[0] + "\n";
//        tv_L.setText(data);

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    //在onResume()內註冊監聽器來使用sensor
    protected void onResume() {
        super.onResume();
        sm.registerListener(this, sr, SensorManager.SENSOR_DELAY_NORMAL);
    }

    protected void onPause() {
        super.onPause();
        sm.unregisterListener(this);
    }


}
