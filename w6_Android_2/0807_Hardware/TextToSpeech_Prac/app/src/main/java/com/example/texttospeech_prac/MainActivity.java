package com.example.texttospeech_prac;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    TextToSpeech tts;
    EditText et;
    Button bt_send;
    Spinner sp_speed, sp_pitch, sp_Lang;

    String[] speed = {"正常速度", "速度*0.5", "速度*2"};
    String[] pitch = {"正常語調", "語調*0.5", "語調*2"};
    String[] language = {"中文", "美式英文", "英式英文", "法文"};

    public void setTextToSpeech() {
        tts = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int i) {

//                int status = 0;
//                if (status == tts.SUCCESS) {
//
//                    int result = 0;
//                    result = tts.setLanguage(Locale.CHINESE);
//
//                    if (result != TextToSpeech.LANG_COUNTRY_AVAILABLE && result != TextToSpeech.LANG_AVAILABLE) {
//                        Toast.makeText(MainActivity.this, "TestToSpeech 不支援此語言", Toast.LENGTH_SHORT).show();
//                    }
//                }
            }
        });
    }

    public void view() {
        et = (EditText) findViewById(R.id.editText);
        bt_send = (Button) findViewById(R.id.button_Send);

        sp_speed = (Spinner) findViewById(R.id.spinner_Speed);
        ArrayAdapter<String> SpeedList = new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_list_item_1, speed);
        sp_speed.setAdapter(SpeedList);

        sp_pitch = (Spinner) findViewById(R.id.spinner_Pitch);
        ArrayAdapter<String> PitchList = new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_list_item_1, pitch);
        sp_pitch.setAdapter(PitchList);

        sp_Lang = (Spinner) findViewById(R.id.spinner_Language);
        ArrayAdapter<String> LangList = new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_list_item_1, language);
        sp_Lang.setAdapter(LangList);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        view();
        setTextToSpeech();


        bt_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // 選擇語言
                switch (sp_Lang.getSelectedItem().toString()) {
                    case "中文":
                        tts.setLanguage(Locale.CHINESE);
                        break;
                    case "美式英文":
                        tts.setLanguage(Locale.US);
                        break;
                    case "英式英文":
                        tts.setLanguage(Locale.UK);
                        break;
                    case "法文":
                        tts.setLanguage(Locale.FRENCH);

                }

                // 選擇速度
                switch (sp_speed.getSelectedItem().toString()) {
                    case "正常速度":
                        tts.setSpeechRate(1);
                        break;
                    case "速度*0.5":
                        tts.setSpeechRate(0.5f);
                        break;
                    case "速度*2":
                        tts.setSpeechRate(2);
                        break;
                }

                // 選擇語調
                switch (sp_pitch.getSelectedItem().toString()) {
                    case "正常語調":
                        tts.setPitch(1);
                        break;
                    case "語調*0.5":
                        tts.setPitch(0.5f);
                        break;
                    case "語調*2":
                        tts.setPitch(2);
                        break;
                }
                
                tts.speak(et.getText().toString(), TextToSpeech.QUEUE_ADD, null);
            }
        });

    }

    public void OnDestory() {
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
    }
}
