package com.example.hardware_tts_hw;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    SensorManager sm;
    Sensor sr;
    TextView tv_XYZ, tv_Status;
    Button bt_Talk;
    TextToSpeech tts;


    public void setAccelerometer() {
        //取得sensor服務並使用
        sm = (SensorManager) getSystemService(SENSOR_SERVICE);
        //TYPE_ACCELEROMETER類型
        sr = sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    }


    public void setTextToSpeech() {
        tts = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int i) {
                tts.setLanguage(Locale.CHINESE);
            }
        });
    }


    public void view() {
        tv_XYZ = (TextView) findViewById(R.id.textView_XYZ);
        tv_Status = (TextView) findViewById(R.id.textView_Status);

        bt_Talk = (Button) findViewById(R.id.button_Talk);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setAccelerometer();
        view();
        setTextToSpeech();

        bt_Talk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tts.speak(tv_Status.getText().toString(), TextToSpeech.QUEUE_ADD, null);
                Log.e("Talk", tv_Status.getText().toString());
            }
        });

    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        float X = sensorEvent.values[0];
        float Y = sensorEvent.values[1];
        float Z = sensorEvent.values[2];
        Log.e("X", String.valueOf(X));

        //XYZ 軸顯示
        tv_XYZ.setText("X軸為：" + sensorEvent.values[0] + "\nY軸為" + sensorEvent.values[1] + "\nZ軸為" + sensorEvent.values[2]);

        if (Math.abs(X) < 6) {
            tv_Status.setText("平放");
        }
        if (Math.abs(Y) > 7) {
            tv_Status.setText("直立");
        }
        if (Math.abs(X) > 6) {
            tv_Status.setText("側放");
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    //在onResume()內註冊監聽器來使用sensor
    protected void onResume() {
        super.onResume();
        sm.registerListener(this, sr, SensorManager.SENSOR_DELAY_NORMAL);
    }

    protected void onPause() {
        super.onPause();
        sm.unregisterListener(this);
    }

}
