package com.company;

public class Main {
    static class Count {
        static int n;
    }

    static Count cou = new Count();
    static class MyThread extends Thread {
        @Override
        public void run() {
            synchronized (cou) {
                for (int i = 0; i < 600; i++) {
                    Count.n += 1;
                    System.out.println(this.getName() + ":" + Count.n);
                }
            }
        }
    }

    public static void main(String[] args) {
        Count count = new Count();

        Thread th1 = new MyThread();
        th1.start();
        Thread th2 = new MyThread();
        th2.start();


        // write your code here
    }
}
