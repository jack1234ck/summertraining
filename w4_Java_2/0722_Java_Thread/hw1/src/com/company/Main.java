package com.company;

import java.util.Calendar;
import java.util.Scanner;

public class Main {

    //計時Thread
    static class CountingThread extends Thread {
        @Override
        public void run() {
            System.out.println("計時開始");

            int second = 0;
            int minute = 0;

            for (int i = 0; i < Integer.MAX_VALUE; i++) {
                if (second > 59) {
                    minute += 1;
                    second = 0;
                }
                second += 1;
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    System.out.println("結束計時");
                    System.out.println("遊戲時間為" + minute + "分" + second + "秒");
                    return;
                }
            }
        }
    }

    //目前時間
    static String CurrentTime() {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(System.currentTimeMillis());
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int min = c.get(Calendar.MINUTE);
        int sec = c.get(Calendar.SECOND);
        return (hour + ":" + min + ":" + sec);
    }

    public static void main(String[] args) {

        System.out.println("目前時間：" + CurrentTime());
        int ans;

        boolean game = true;
        Thread Count = new CountingThread();
        Count.start();

        ans = (int) (Math.random() * 99 + 1);
        System.out.println("答案為：" + ans);

        while (game) {

            System.out.println("請輸入數字：");
            Scanner sc = new Scanner(System.in);
            int guess = sc.nextInt();

            if (guess > 99 || guess < 1) {
                System.out.println("必須介於1~99之間喔");
            } else if (guess > ans) {
                System.out.println("太大了喔");
            } else if (guess < ans) {
                System.out.println("太小了喔");
            } else if (guess == ans) {
                System.out.println("答對了！");
                System.out.println("答案為：" + guess);
                game = false;
            }
        }
        if (!game) {
            System.out.println("結束時間：" + CurrentTime());
            Count.interrupt();
            System.out.println("Game Over.");
        }

    }
}
