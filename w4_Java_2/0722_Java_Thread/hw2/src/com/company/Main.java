package com.company;

import java.util.Calendar;
import java.util.Scanner;

public class Main {

    //目前時間
    static String CurrentTime() {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(System.currentTimeMillis());
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int min = c.get(Calendar.MINUTE);
        int sec = c.get(Calendar.SECOND);
        String time = null;
        if (min < 10 && sec < 10) {
            time = hour + ":0" + min + ":0" + sec;
        }
        if (min < 10) {
            time = hour + ":0" + min + ":" + sec;
        } else if (sec < 10) {
            time = hour + ":" + min + ":0" + sec;
        } else {
            time = hour + ":" + min + ":" + sec;
        }
        return time;
    }

    static class ShowThread extends Thread {
        int sleep = 0;

        @Override
        public void run() {
            for (int i = 0; i < Integer.MAX_VALUE; i++) {
                try {
                    if (sleep == 0) {
                        Thread.sleep(1000);
                        System.out.println("目前時間" + CurrentTime());
                    } else {
                        System.out.println("（為" + sleep + "秒顯示一次。）");
                        Thread.sleep(sleep * 1000);
                        System.out.println("目前時間" + CurrentTime());
                        //sleep = 0;
                    }
                } catch (InterruptedException e) {
                    return;
                }
            }
        }
    }

    public static void main(String[] args) {

        Thread show = new ShowThread();
        boolean mode = true;
        show.start();

        while (mode) {
            Scanner sc = new Scanner(System.in);
            String incall = sc.next();
            if (incall.equals("stop")) {
                show.interrupt();
                mode = false;
            } else {
                int sleepnum = Integer.parseInt(incall);
                ((ShowThread) show).sleep = sleepnum;
            }
        }

        System.out.println("System Over.");

    }
}
