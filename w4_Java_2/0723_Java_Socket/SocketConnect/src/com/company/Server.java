package com.company;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Server {

    static Socket sok;

    public static class ServerOut extends Thread {
        DataOutputStream outStream;
        String SendText = null;

        @Override
        public void run() {
            try {
                while (true) {
                    if (SendText != "") {
                        outStream = new DataOutputStream(sok.getOutputStream());
                        //System.out.print("輸入傳送資訊：");
                        Scanner Scin = new Scanner(System.in);
                        SendText = Scin.next();
                        outStream.writeUTF(SendText);
                    }
                }
            } catch (IOException e) {
                System.out.println("ServerOut Error");
                e.printStackTrace();
            }
        }
    }

    public static class ServerIn extends Thread {
        DataInputStream inStream;
        String ReceivedText;

        @Override
        public void run() {
            try {
                while (true) {
                    if (ReceivedText != "") {
                        inStream = new DataInputStream(sok.getInputStream());
                        ReceivedText = inStream.readUTF();
                        System.out.println("客戶端:" + ReceivedText);
                        if (ReceivedText.equals("stop")){
                            System.out.println("System exit");
                            System.exit(2);
                        }
                    }
                }
            } catch (IOException e) {
                System.out.println("ServerIn Error");
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {

        ServerIn sthin = new ServerIn();
        ServerOut sthout = new ServerOut();

        try {

//            Socket sok = new Socket("www.google.com",80);
//
//            System.out.println("本機位址：" + sok.getLocalAddress());
//            System.out.println("本機Port：" + sok.getLocalPort());
//            System.out.println("遠端位址：" + sok.getInetAddress());
//            System.out.println("遠端Port：" + sok.getPort());

            ServerSocket SerSok = new ServerSocket(8000);
            System.out.println("開始聆聽");

            sok = SerSok.accept();
            System.out.println("客戶端已連線");

            if (sthin.ReceivedText != "") {
                sthout.start();
                sthin.start();
            }

        } catch (IOException e) {
            System.out.println("ERROR");
        }
        // write your code here
    }
}
