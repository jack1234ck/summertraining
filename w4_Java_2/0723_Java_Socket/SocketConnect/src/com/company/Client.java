package com.company;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class Client {

    static Socket Client;

    public static class ClientIn extends Thread {
        DataInputStream inStream;
        String ReText;

        @Override
        public void run() {
            try {
                while (true) {
                    if (ReText != "") {
                        inStream = new DataInputStream(Client.getInputStream());
                        ReText = inStream.readUTF();
                        System.out.println("伺服端：" + ReText);
                    }
                }

            } catch (IOException e) {
                System.out.println("ClientIn Error");
                e.printStackTrace();
            }
        }
    }

    public static class ClientOut extends Thread {
        String text = null;
        DataOutputStream outStream;

        @Override
        public void run() {

            try {
                while (true) {
                    if (text != "") {
                        outStream = new DataOutputStream(Client.getOutputStream());
                        Scanner sc = new Scanner(System.in);
                        text = sc.next();
                        outStream.writeUTF(text);
                        if (text.equals("stop")){
                            System.out.println("System Exit");
                            System.exit(2);
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {

        ClientIn clientIn = new ClientIn();
        ClientOut clientOut = new ClientOut();

        try {

            Client = new Socket("127.0.0.1", 8000);

            if (clientOut.text != "") {
                clientOut.start();
                clientIn.start();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}

