package com.company;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class server {
    static Socket serve;
    static DataInputStream inputStream;
    static DataOutputStream outputStream;
    static String[][] maps;

    public static void main(String[] args) {
        try {
            ServerSocket serverSocket = new ServerSocket(5000);
            System.out.println("Four_Chess Server Start");
            serve = serverSocket.accept();
            inputStream = new DataInputStream(serve.getInputStream());
            outputStream = new DataOutputStream(serve.getOutputStream());
            System.out.println("連接用戶為" + serve.getInetAddress());
            outputStream.writeUTF("1:遊戲開始 遊戲說明如下");
            outputStream.writeUTF("1:有遊戲為6*7的地圖");
            outputStream.writeUTF("1:輸入排數填入值到最底部");
            outputStream.writeUTF("1:電腦會以亂數來遊玩");
            outputStream.writeUTF("1:請愉快地玩遊戲");
            outputStream.writeUTF("1:輸入8 結束遊戲 輸入9重新開始");
            maps = new String[6][7];
            reset();
            displaymaps();
            outputStream.writeUTF("2:0");
            while (serve.isConnected()) {
                int getclientans = inputStream.readInt();
                if (getclientans == 8) {
                    System.out.println("END");
                    serve.close();
                    System.exit(0);
                } else if (getclientans <= 7) {
                    game(getclientans - 1, "O");
                    game((int) ((Math.random() * 7)), "X"); //電腦隨機挑選排數，下 "X"
                    displaymaps();
                    outputStream.writeUTF(verification());

                } else if (getclientans == 9) {
                    System.out.println("RESTART");
                    reset();
                    displaymaps();
                    outputStream.writeUTF("1:已經重新開始");
                    outputStream.writeUTF("2:0");
                } else {
                    outputStream.writeUTF("1:輸入錯誤 請重新輸入");
                    outputStream.writeUTF("2:0");
                }
            }

        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }

    public static void game(int x, String star) {
        for (int i = 5; i >= 0; i--) {
            if (maps[i][x].equals(" ")) {
                maps[i][x] = star;
                break;
            } else {
                continue;
            }
        }
    }

    public static String verification() {
        int clientwinrow, cpwinrow, cpwincol, clientcol, clientoblique, cpoblique, clientreoblique, cporeblique;
        clientwinrow = cpwinrow = cpwincol = clientcol = clientoblique = cpoblique = cporeblique = clientreoblique = 0; //初始化
        for (int i = 2; i >= 0; i--) {
            for (int j = 3; j >= 0; j--) {
                for (int k = 0; k < 4; k++) {
                    for (int l = 0; l < 4; l++) {
                        if (maps[i + k][j + l].equals("O")) {          //驗證「橫」   OOOO
                            clientwinrow++;
                        }
                        if (maps[i + k][j + l].equals("X")) {          //驗證「橫」   XXXX
                            cpwinrow++;
                        }
                        if (maps[i + l][j + k].equals("O")) {       //驗證「直」 Ex  X or O
                            clientcol++;                            //             X    O
                        }                                           //             X    O
                        if (maps[i + l][j + k].equals("X")) {       //             X    O
                            cpwincol++;
                        }
                    }
                    if (clientwinrow == 4 || clientcol == 4) {
                        return "3:0";
                    }
                    if (cpwinrow == 4 || cpwincol == 4) {
                        return "3:1";
                    }
                    clientwinrow = cpwinrow = cpwincol = clientcol = 0;
                }
                for (int k = 0; k < 4; k++) {
                    for (int l = 0; l < 4; l++) {
                        if (maps[i + l][j + l].equals("O")) {               //驗證「右斜」 Ex: X   or  O
                            clientoblique++;                                //                X        O
                        }                                                   //                 X         O
                        if (maps[i + l][j + l].equals("X")) {               //                  X          O
                            cpoblique++;
                        }
                        if (maps[i + l][j + (3 - l)].equals("O")) {         //驗證「左斜」 Ex:X   or  O
                            clientreoblique++;                              //          X       O
                        }                                                   //        X       O
                        if (maps[i + l][j + (3 - l)].equals("X")) {         //      X       O
                            cporeblique++;
                        }
                    }
                    if (clientoblique == 4 || clientreoblique == 4) {
                        return "3:0";
                    }
                    if (cporeblique == 4 || cpoblique == 4) {
                        return "3:1";
                    }
                    clientoblique = cpoblique = clientreoblique = cporeblique = 0;
                }
            }
        }
        return "2:0";
    }

    public static void reset() {
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 7; j++) {
                maps[i][j] = " ";
            }
        }
    }

    public static void displaymaps() {
        try {
            outputStream.writeUTF("1:" + "| 1 | 2 | 3 | 4 | 5 | 6 | 7 |");
        }catch (Exception e){
        }
        System.out.println("| 1 | 2 | 3 | 4 | 5 | 6 | 7 |");
        String temp = "| ";
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 7; j++) {
                temp += maps[i][j] + " | ";
            }
            System.out.println(temp);
            try {
                outputStream.writeUTF("1:" + temp.toString());
            } catch (Exception e) {
                System.out.println(e.toString());
            }
            temp = "| ";
        }
        System.out.println("-------------------------------");
    }
}