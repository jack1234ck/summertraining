package com.company;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.util.Scanner;

public class client {
    static Socket client;

    public static void main(String[] args) {
        try {
            Scanner sc = new Scanner(System.in);
            client = new Socket("127.0.0.1", 5000);
            DataOutputStream outputStream = new DataOutputStream(client.getOutputStream());
            DataInputStream inputStream = new DataInputStream(client.getInputStream());
            System.out.println("Client connect");
            while (client.isConnected()) {
                String[] receive = inputStream.readUTF().split(":");
                int status = Integer.parseInt(receive[0]);
                if (status == 1) {
                    System.out.println(receive[1]);
                } else if (status == 2) {
                    System.out.print("請輸入第幾排:");
                    int rows = sc.nextInt();
                    while (rows > 9 || rows < 0) {
                        System.out.print("輸入錯誤，請輸入第幾排");
                        rows = sc.nextInt();
                    }
                    outputStream.writeInt(rows);
                } else if (status == 3) {
                    if (receive[1].equals("0")){
                        System.out.println("恭喜你贏了!!");
                    }else {
                        System.out.println("電腦贏了 繼續加油!!");
                    }
                    System.out.println("輸入 '8' 結束遊戲；輸入 '9' 重新開始");
                    System.out.print("輸入（8/9）:");
                    int rows = sc.nextInt();
                    outputStream.writeInt(rows);
                    if (rows==8){
                        System.out.println("遊戲結束");
                        client.close();
                        System.exit(0);
                    }
                } else {
                    System.out.println("Error Get");
                }
            }
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }

}