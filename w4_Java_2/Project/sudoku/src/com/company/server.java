package com.company;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class server {
    static Socket serve;
    static DataInputStream inputStream;
    static DataOutputStream outputStream;
    static String[][] maps;
    static String[][][] topic;


    public static void main(String[] args) {
        try {
            ServerSocket serverSocket = new ServerSocket(1503);
            System.out.println("Sudoku Server Start");
            serve = serverSocket.accept();
            inputStream = new DataInputStream(serve.getInputStream());
            outputStream = new DataOutputStream(serve.getOutputStream());
            System.out.println("連接用戶為" + serve.getInetAddress());

            outputStream.writeUTF("1:\n");
            outputStream.writeUTF("1:數獨遊戲開始");
            outputStream.writeUTF("1:遊戲說明如下：");
            outputStream.writeUTF("1:遊戲為9*9的地圖");
            outputStream.writeUTF("1:輸入排數和數值來完成數獨");
            outputStream.writeUTF("1:輸入格式為：（ '行' ,  '列' , '數字 '）");
            outputStream.writeUTF("1:輸入 '10,0,0' 結束遊戲");
            outputStream.writeUTF("1:輸入 '11,0,0' 重新開始");
            outputStream.writeUTF("1:請愉快地玩遊戲");
            outputStream.writeUTF("1:\n");


            maps = new String[9][9];

            reset();
            display();

            outputStream.writeUTF("2:0");

            while (serve.isConnected()) {
                int x = inputStream.readInt();
                int y = inputStream.readInt();
                int z = inputStream.readInt();
                System.out.println("玩家輸入" + x + "," + y + "," + z);

                if (x <= 9 && y <= 9) {
                    System.out.println();
                    String re = verification(x - 1, y - 1, z);
                    display();
                    if (re.equals("4:1")) {
                        outputStream.writeUTF("4:1");
                    } else if (re.equals("4:0")) {
                        outputStream.writeUTF("4:0");
                    } else if (re.equals("4:2")) {
                        outputStream.writeUTF("4:2");
                    } else if (re.equals("3:0")) {
                        outputStream.writeUTF("3:0");
                    } else {
                        outputStream.writeUTF("2:0");
                    }
                } else if (x == 10) {
                    System.out.println("遊戲結束");
                    serve.close();
                    System.exit(0);
                } else if (x == 11) {
                    reset();
                    display();
                    outputStream.writeUTF("1:已經重新開始");
                    outputStream.writeUTF("2:0");
                } else {
                    outputStream.writeUTF("1:輸入錯誤 請重新輸入");
                    outputStream.writeUTF("2:0");
                }
            }

        } catch (Exception e) {
            //System.out.println(e.toString());
        }
    }

    static int t = (int) (Math.random() * 3);

    public static void reset() { //存放題目
        topic = new String[3][9][9];
        topic[0] = new String[][]{
                {"1", "4", "7", "2", "5", "3", "9", "6", "8"},
                {"2", "3", "8", "4", "6", "9", "1", "7", "5"},
                {"6", "9", "5", "7", "8", "1", "2", "3", "4"},
                {"4", "5", "6", "1", "3", "8", "7", "2", "9"},
                {"3", "7", "9", "6", "2", "4", "5", "8", "1"},
                {"8", "1", "2", "5", "9", "7", "3", "4", "6"},
                {"9", "2", "3", "8", "1", "6", "4", "5", "7"},
                {"5", "6", "4", "9", "7", "2", "8", "1", "3"},
                {"7", "8", "1", "3", "4", "5", "6", "9", "2"}
        };

        topic[1] = new String[][]{
                {"6", "1", "5", "2", "3", "4", "7", "9", "8"},
                {"4", "3", "2", "8", "7", "9", "6", "1", "5"},
                {"8", "7", "9", "1", "5", "6", "2", "4", "3"},
                {"2", "9", "7", "4", "8", "1", "3", "5", "6"},
                {"1", "8", "3", "6", "2", "5", "9", "7", "4"},
                {"5", "4", "6", "3", "9", "7", "8", "2", "1"},
                {"7", "2", "8", "5", "1", "3", "4", "6", "9"},
                {"9", "5", "4", "7", "6", "8", "1", "3", "2"},
                {"3", "6", "1", "9", "4", "2", "5", "8", "7"}
        };

        topic[2] = new String[][]{
                {"5", "1", "3", "2", "6", "4", "7", "9", "8"},
                {"7", "6", "4", "8", "1", "9", "2", "3", "5"},
                {"2", "9", "8", "7", "3", "5", "1", "4", "6"},
                {"1", "4", "6", "3", "7", "2", "8", "5", "9"},
                {"3", "8", "2", "5", "9", "6", "4", "7", "1"},
                {"9", "5", "7", "1", "4", "8", "6", "2", "3"},
                {"4", "7", "1", "9", "8", "3", "5", "6", "2"},
                {"6", "3", "5", "4", "2", "1", "9", "8", "7"},
                {"8", "2", "9", "6", "5", "7", "3", "1", "4"}
        };

        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                maps[i][j] = topic[t][i][j];
            }
        }

        int n, m;
        for (int i = 0; i < 43; i++) { //從題目中挖出43個空格，供玩家作答
            n = (int) (Math.random() * 9);
            m = (int) (Math.random() * 9);
            maps[n][m] = " ";
        }
    }

    public static void display() {
        String line = "  _____ _____ _____ ";
        String print = "";
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                if (maps[i][j].equals(" ")) {
                    System.out.print("* ");
                    print += "* ";
                } else {
                    System.out.print(maps[i][j] + " ");
                    print += maps[i][j] + " ";
                }
            }
            try {
                if (i == 0) {
                    outputStream.writeUTF("1:  1 2 3 4 5 6 7 8 9 ");
                }
                int n = i % 3;
                if (n == 0) {
                    outputStream.writeUTF("1:" + line);
                }
                outputStream.writeUTF("1:" + (i + 1) + "|" + print + "|");
                print = "";
                System.out.println();
            } catch (Exception e) {
            }
        }
        try {
            outputStream.writeUTF("1:" + line + "\n");
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public static String verification(int x, int y, int number) {

        // 2:0 判斷沒問題 可繼續
        // 3:0 使用者獲勝
        // 4:0 為輸入錯誤 (不能更改題目)
        // 4:1 數值輸入錯誤
        // 4:2 與答案不符合


        // 4:0 為輸入錯誤(不能更改題目)
        if (!maps[x][y].equals(" ")) {
            return "4:0";
        }

        int full = 0;
        int[] HorizontalNumber = new int[9];
        int[] VerticallNumber = new int[9];
        int[] SquareNumber = new int[9];
        for (int i = 0; i < 9; i++) {  //初始化
            HorizontalNumber[i] = 0;
            VerticallNumber[i] = 0;
            SquareNumber[i] = 0;
        }

        maps[x][y] = Integer.toString(number);
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                if (!maps[i][j].equals(" ")) {          // 驗證橫的 EX 123456789
                    full++;
                    HorizontalNumber[Integer.parseInt(maps[i][j]) - 1] += 1;
                }

                if (!maps[j][i].equals(" ")) {                               // 直的 Ex 1
                    VerticallNumber[Integer.parseInt(maps[j][i]) - 1] += 1;  //        2
                }                                                            //        3

                if (j % 3 == 0 && i % 3 == 0) {                                     //減少判斷 只在 j=0 3 6 執行

                    for (int k = 0; k < 3; k++) {                            // 九宮格 Ex 1  4  7
                        for (int l = 0; l < 3; l++) {                        //       Ex 2  5  8
                            if (!maps[k + i][l + j].equals(" ")) {           //       Ex 3  6  9
                                SquareNumber[Integer.parseInt(maps[k + i][l + j]) - 1] += 1;
                            }
                        }
                    }
                    for (int k = 0; k < 9; k++) {    //九宮格 驗算

                        if (SquareNumber[k] >= 2) {  //如果有超過2個以上 直接return
                            maps[x][y] = " ";
                            return "4:1";            // 4:1 數值輸入錯誤 (「九宮格」中已有相同的數字)
                        }
                    }
                    for (int k = 0; k < 9; k++) {    //清空
                        SquareNumber[k] = 0;
                    }
                }
            }
            for (int j = 0; j < 9; j++) {
                if (HorizontalNumber[j] >= 2) {
                    maps[x][y] = " ";
                    return "4:1";                    // 4:1 數值輸入錯誤 (「同一行」中已有相同的數字)
                }
                if (VerticallNumber[j] >= 2) {
                    maps[x][y] = " ";
                    return "4:1";                    // 4:1 數值輸入錯誤 (「同一列」中已有相同的數字)
                }
            }
            for (int j = 0; j < 9; j++) {
                HorizontalNumber[j] = 0;
                VerticallNumber[j] = 0;
            }
            if (full == 81) {
                return "3:0";                        // 3:0 使用者獲勝
            }
        }

        if (!maps[x][y].equals(topic[t][x][y])) {
            maps[x][y] = " ";
            return "4:2";                            // 4:2 與答案不符合
        }

        return "2:0";                                // 2:0 上述的條件都未觸發的的話，表示輸入正確的數值，並繼續遊戲。
    }

}
