package com.company;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.util.Scanner;

public class client {
    static Socket client;

    public static void main(String[] args) {
        try {
            Scanner sc = new Scanner(System.in);
            client = new Socket("127.0.0.1", 1503);
            DataOutputStream outputStream = new DataOutputStream(client.getOutputStream());
            DataInputStream inputStream = new DataInputStream(client.getInputStream());
            System.out.println("Client connect");
            while (client.isConnected()) {
                String[] receive = inputStream.readUTF().split(":");
                int status = Integer.parseInt(receive[0]);
                if (status == 1) {
                    System.out.println(receive[1]);
                } else if (status == 2) {
                    System.out.print("請輸入（ '行' ,  '列' , '猜測的數字' ）:");
                    // 5,6,8
                    String rows = sc.next();
                    String[] context = rows.split(",");
                    int a = Integer.parseInt(context[0]); // Y軸
                    int b = Integer.parseInt(context[1]); // X軸
                    int c = Integer.parseInt(context[2]); // 猜測的數字
                    outputStream.writeInt(a);
                    outputStream.writeInt(b);
                    outputStream.writeInt(c);
                } else if (status == 3) {
                    if (receive[1].equals("0")){
                        System.out.println("恭喜你贏了!!");
                    }
                    System.out.print("輸入10,0,0 結束遊戲 輸入11,0,0重新開始");
                    String rows = sc.next();
                    String[] context = rows.split(",");
                    int a = Integer.parseInt(context[0]);
                    int b = Integer.parseInt(context[1]);
                    int c = Integer.parseInt(context[2]);
                    outputStream.writeInt(a);
                    outputStream.writeInt(b);
                    outputStream.writeInt(c);

                    if (a == 10){
                        System.out.println("遊戲結束");
                        client.close();
                        System.exit(0);
                    }
                } else if (status == 4) {
                    if (receive[1].equals("0")) {
                        System.out.println("不允許更改題目");
                    }else if (receive[1].equals("1")){
                        System.out.println("數值輸入錯誤(行、列、3*3已經出現過這個數字ㄌ)");
                    } else if (receive[1].equals("2")){
                        System.out.println("與答案不符");
                    }
                    System.out.print("請重新輸入:");
                    String rows = sc.next();
                    String[] context = rows.split(",");
                    int a = Integer.parseInt(context[0]);
                    int b = Integer.parseInt(context[1]);
                    int c = Integer.parseInt(context[2]);
                    outputStream.writeInt(a);
                    outputStream.writeInt(b);
                    outputStream.writeInt(c);
                    if (a==10){
                        System.out.println(a);
                        client.close();
                        System.exit(0);
                    }
                } else {
                    System.out.println("Error Get");
                }
            }
        } catch (Exception e) {
            //System.out.println(e.toString());
        }
    }

}
