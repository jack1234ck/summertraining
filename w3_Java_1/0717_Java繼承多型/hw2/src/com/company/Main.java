package com.company;

public class Main {

    public static abstract class Animal {

        String name;

        public Animal() {
            name = "animal";
        }

        public void setName(String n) {
            name = n;
        }

        //函式採用「抽象 abstract」方法來定義
        public abstract void move();

        public abstract void sound();

    }

    public static void main(String[] args) {

        Animal doooog = new Animal() {

            //再利用「匿名類別」的方式來重新建立move、sound定義的內容
            public void move() {
                System.out.println("Move:Walk");
            }

            public void sound() {
                System.out.println("Sound:rrrrrr");
            }

        };

        Animal caaaat = new Animal() {
            @Override
            public void move() {
                System.out.println("Move:Slowly Walk");

            }

            @Override
            public void sound() {
                System.out.println("Sound:Meow");
            }
        };

        doooog.setName("Dog Peter");
        doooog.move();
        doooog.sound();

        System.out.println("=============");

        caaaat.setName("Cat John");
        caaaat.move();
        caaaat.sound();

    }

}

