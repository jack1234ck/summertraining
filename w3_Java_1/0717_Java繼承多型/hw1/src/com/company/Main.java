package com.company;

import java.util.Scanner;

public class Main {

    String Name;

    //宣告存款餘額 "Balance"
    public static int Balance = 0;

    //父類別Account
    public static class Account {

        //進行存款 "Credit" 函式
        public void Credit() {
            System.out.println("輸入需存入金額");
            Scanner sc = new Scanner(System.in);
            int CreditMoney = 0;
            CreditMoney = sc.nextInt();
            Balance += CreditMoney;
        }

        //進行提款 "Debit" 函式
        public void Debit() {
            System.out.println("輸入需領出金額");
            Scanner sc = new Scanner(System.in);
            int DebitMoney = 0;
            DebitMoney = sc.nextInt();
            Balance += DebitMoney;
        }

    }

    //宣告利率
    static double InterestRate;

    //子類別SavingAccount (儲蓄帳戶)
    public static class SavingAccount extends Account {


        //讀取利率
        public double GetInterestRate() {
            System.out.println("利率為：" + InterestRate);
            return InterestRate;
        }

        //設定利率
        public void SetIntersetRate() {
            System.out.println("輸入利率");
            Scanner sc = new Scanner(System.in);
            InterestRate = sc.nextDouble();
            GetInterestRate();
        }

        //計算利息並加入存款
        public void CalculateInterest() {
            int Interest, Money;
            System.out.print("輸入需存入金額：");
            Scanner sc = new Scanner(System.in);
            Money = sc.nextInt();
            Interest = (int) (Money * InterestRate);
            System.out.println("存入金額：" + Money);
            System.out.println("現今利率為：" + InterestRate);
            System.out.println("計算後利息：" + Interest);
            Balance += (Interest + Money);
        }

    }

    //子類別CheckAccount(支票帳號)
    public static class CheckAccount extends Account {

        //宣告存提款手續費 "TransactionFee"
        public double TransactionFee;
        public int Money;
        public double Rate;


        //重新定義「存款 "Credit" 函式」以符合手續費收取
        @Override
        public void Credit() {
            ChargeFee();
            Balance += (Money - TransactionFee);
            System.out.println("已存入金額：" + Money);
            System.out.println("手續費為：" + TransactionFee);
            System.out.println("剩餘存款：" + Balance);

        }

        //重新定義「提款 "Debit" 函式」以符合手續費收取
        @Override
        public void Debit() {
            ChargeFee();
            Balance -= (Money + TransactionFee);
            System.out.println("已領出金額：" + Money);
            System.out.println("手續費為：" + TransactionFee);
            System.out.println("剩餘存款：" + Balance);
        }

        //設計一個工具函式 "ChargeFee" 來協助費率的計算
        public void ChargeFee() {
            System.out.println("輸入金額");
            Scanner sc = new Scanner(System.in);
            Money = sc.nextInt();

            System.out.println("輸入費率");
            Rate = sc.nextDouble();

            TransactionFee = Money * Rate;

            System.out.println("金額為：" + Money);
            System.out.println("費率為：" + Rate);
        }
    }

    public static void main(String[] args) {

        String Name;
        int CurrentBalence;

        System.out.println("歡迎使用銀行帳戶");
        System.out.println("請輸入帳戶名稱");
        Scanner sc = new Scanner(System.in);
        Name = sc.next();
        Account One = new Account();

        System.out.println("請輸入預有帳戶金額");
        sc = new Scanner(System.in);
        CurrentBalence = sc.nextInt();
        if (CurrentBalence >= 0) {
            Balance = CurrentBalence;
        }

        Boolean Power = true;

        while (Power) {

            String Mode = "";

            System.out.println("-------------------------");
            System.out.println("名稱：" + Name);
            System.out.println("總存款：" + Balance);
            System.out.println("-------------------------");
            System.out.println("1:設定儲蓄帳戶利率         ");
            System.out.println("2:計算利息並存款儲蓄帳戶    ");
            System.out.println("-------------------------");
            System.out.println("3:存款至支票帳戶           ");
            System.out.println("4:提款從支票帳戶           ");
            System.out.println("-------------------------");
            System.out.println("88:結束　　　　　　　　　　　");
            System.out.println("-------------------------");


            System.out.print("輸入需執行工作代碼：");
            sc = new Scanner(System.in);
            Mode = sc.next();

            switch (Mode) {
                case "1":
                    SavingAccount SAOne = new SavingAccount();
                    SAOne.SetIntersetRate();
                    Power = true;
                    break;
                case "2":
                    SAOne = new SavingAccount();
                    SAOne.CalculateInterest();
                    Power = true;
                    break;
                case "3":
                    CheckAccount CAOne = new CheckAccount();
                    CAOne.Credit();
                    Power = true;
                    break;
                case "4":
                    CAOne = new CheckAccount();
                    CAOne.Debit();
                    Power = true;
                    break;
                case "88":
                    System.out.println("掰掰");
                    Power = false;
                    break;
                default:
                    Power = true;
                    break;
            }
        }

    }

}
