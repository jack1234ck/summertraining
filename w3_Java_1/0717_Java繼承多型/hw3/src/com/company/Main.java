package com.company;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Main {

    //新增類別「帳號 "Account"」
    public static class Account<Account> {
        String N;
        int B;

        //定義「名字 "name"」與「餘額 "balance"」
        public Account(String name, int balance) {
            N = name;
            B = balance;
        }

        protected String getName() {
            return N;
        }

        protected int getBalence() {
            return B;
        }

        //輸出字串格式化
        public String toString() {
            return N + ":" + B;
        }
    }

    public static void main(String[] args) {

        List<Account> AccountList = new ArrayList<Account>();

        //新增五個實體資料內容
        AccountList.add(new Account("Mary", 30));
        AccountList.add(new Account("Gary", 8));
        AccountList.add(new Account("Tery", 66));
        AccountList.add(new Account("Ben", 1200));
        AccountList.add(new Account("Alice", 18));

        System.out.println("排序前");
        //顯示 "AccountList" 排序前資料
        for (Object obj : AccountList) {
            System.out.println(obj);
        }


        System.out.println("依照餘額排序後");
        //利用「餘額 "balance"」排序
        AccountList.sort(Comparator.<Account, Integer>comparing(p -> p.B));
        //顯示 "AccountList" 依照「餘額 "balance"」排序後的資料
        for (Object obj : AccountList) {
            System.out.println(obj);
        }

        System.out.println("依照姓名排序後");
        //利用「名字 "name" 」排序
        AccountList.sort(Comparator.<Account, String>comparing(p -> p.N));
        //顯示 "AccountList" 依照「名字 "name" 」排序後的資料
        for (Object obj : AccountList) {
            System.out.println(obj);
        }

    }
}

//https://openhome.cc/Gossip/Java/ComparableComparator.html
