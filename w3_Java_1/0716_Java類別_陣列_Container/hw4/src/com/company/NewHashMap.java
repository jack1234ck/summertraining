package com.company;

public class NewHashMap{
    public int timezone = 0;

    public void getTimeZone(String place) { // Object Method

        String Location;
        // 利用Split來分開經度與緯度
        String[] split_location = place.split("[,°]");

        //location[2]為經度，除於15得出時區。
        timezone = Integer.parseInt(split_location[2])/15;

        //辨別是否為西經或東經，如果為西經的話時區為負
        if(split_location[3].indexOf("W") != -1){
            timezone *= -1;
        }
        System.out.println("時區為:"+timezone);
    }

}
