package com.company;

import java.util.HashMap;
import java.util.Scanner;

public class Main {


    public static void main(String[] args) {

        HashMap<String, String> Location = new HashMap<String, String>();

        Location.put("東京迪士尼", "35°37N,139°52E");
        Location.put("台北101", "25°02N,121°33E");
        Location.put("法國艾菲爾鐵塔", "48°51N,2°17E");
        Location.put("美國黃石國家公園", "44°36N,110°30W");

        //輸入一個地點，輸出該地點的經緯度
        System.out.println("輸入需查詢之地點");
        Scanner sc = new Scanner(System.in);
        String name;
        name = sc.next();
        System.out.println("經緯度為：" + Location.get(name));
        System.out.println("======================");

        //輸入兩個點來計算這兩個點的時差多少
        String place1, place2, place1sLocation, place2sLocation;
        int time;

        System.out.println("輸入第一個地點");
        place1 = sc.next();
        place1sLocation = Location.get(place1);
        NewHashMap p1 = new NewHashMap();
        p1.getTimeZone(place1sLocation);


        System.out.println("輸入第二個地點");
        place2 = sc.next();
        place2sLocation = Location.get(place2);
        NewHashMap p2 = new NewHashMap();
        p2.getTimeZone(place2sLocation);

        time = Math.abs(p1.timezone - p2.timezone);
        System.out.println("「"+place1+"」與「"+place2+"」相差"+time+"個小時。");

    }
}

//    東京迪士尼
//    台北101
//    法國艾菲爾鐵塔
//    美國黃石國家公園