package com.company;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        String newData;

        List<String> data = new ArrayList<String>();
        List<String> LinkData = new LinkedList<String>();

        System.out.print("輸入新資料：");
        Scanner sc = new Scanner(System.in);
        newData = sc.next();

        //新增資料在ArrayList裡
        data.add("Data1");
        data.add("Data2");
        data.add("Data3");
        data.add("Data4");
        data.add("Data5");
        data.add("Data6");
        data.add(newData);

        //新增資料在LinkedList裡
        LinkData.add("LinkedData1");
        LinkData.add("LinkedData2");
        LinkData.add("LinkedData3");
        LinkData.add("LinkedData4");
        LinkData.add("LinkedData5");
        LinkData.add("LinkedData6");
        LinkData.add(newData);

        //Stack 堆疊 後進先出
        System.out.println("Stack 堆疊");
        for (int i = data.size() - 1; i >= 0; i--) {
            System.out.println(data.get(i));
        }
        System.out.println("------");

        //Queue 佇列 先進先出
        System.out.println("Queue 佇列");
        for (int i = 0; i < LinkData.size(); i++) {
            System.out.println(LinkData.get(i));
        }
    }
}
