package com.company;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        ArrayList<Integer> num = new ArrayList<Integer>();

        Boolean StopSign = true;
        int i = 0;
        int all = 0;


        //用無限迴圈連續讀取整數
        while (StopSign) {
            Scanner sc = new Scanner(System.in);
            int innum;
            innum = sc.nextInt();

            //直到讀到0才結束
            if (innum == 0) {
                StopSign = false;
                break;
            } else {
                //加至ArrayList裡面
                num.add(innum);
            }
        }

        //計算所有整數的總和
        for (int p = 0; p < num.size(); p++) {
            all += num.get(p);
            System.out.println("第" + (p + 1) + "個數為：" + num.get(p));
        }


        //輸出總和
        System.out.println("總和為" + all);

        // write your code here
    }
}
