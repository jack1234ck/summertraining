package com.company;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        ArrayList<Integer> num = new ArrayList<Integer>();

        // 將1~500的整數放入其中
        for (int i = 0; i < 500; i++) {
            num.add(i, i + 1);
        }


        for (int i = 0; i < num.size(); i++) {
            //將所有 4 的倍數的資料 Remove
            if (num.get(i) % 4 == 0) {
                num.remove(i);

            }
        }
        for (int i = 0; i < num.size(); i++) {
            System.out.println(num.get(i));
        }

    }
}
